Proyecto de automatización de pruebas para las apps móviles con Appium, Selenium, TestNG.
Queda pendiente el desarrollo en cucumber.

Investigar la posiblidad de integrar en Docker emuladores de android (al menos) con el objetivo de crear una granja de móviles (emuladores).

Se comienza con la creación de una suite de test 'smoke test' con los casos de uso más habituales con el objetico de asegurar un mínimo de calidad en la entrega y evitar errores críticos.
Se ha visto la posiblidad de crear una suite de test para verificar las Keys y sus traducciones (dependerá de si los elementos se pueden identificar correctamente con Appium).
_______________________

Requerimientos previos
Instalar:

Maven : https://maven.apache.org/
Appium Desktop : https://github.com/appium/appium-desktop
Editor Java, en este caso se utilizará IntelliJ.
Emulador o dispositivo físico Android.


Configurar las variables de entorno de Android, Java, Maven en el sistema y añadirlas al PATH:
ANDROID_HOME
JAVA_HOME
M2 : C:\Maven\bin
M2_HOME : C:\Maven


Construcción del Proyecto Maven:
Respositorio oficial de Maven: https://mvnrepository.com/

crear un nuevo proyecto de tipo Maven
configurar las dependencias en el fichero "pom.xml"
añadimos las siguientes dependencias:
Appium
Junit
Cucumber
compilar el proyecto para verificar que no existan errores.