package es.odilo.odilotest.stepsDefinition.A_pantalla_Login;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.URL;

public class BaseLoginClass {

    // Atributos de la clase para montar el driver
    private String deviceName;
    private String uuid;
    private String platformName;
    private String platformVersion;
    private String appPackage;
    private String appActivity;
    private String appiumUrl;


    // Constructor con el mismo nombre de la clase
    public BaseLoginClass(){}

    // Métodos de la clase
    // get set deviceName
    public String getDeviceName() {
        return deviceName;
    }
    public void setDeviceName(String atrDeviceName) {
        deviceName = atrDeviceName;
    }

    // get set UUID
    public String getUuid() {
        return uuid;
    }
    public void setUuid(String atrUuid) {
        uuid = atrUuid;
    }

    // get set platformName
    public String getPlatformName(){
        return platformName;
    }
    public void setPlatformName(String atrPlatformName) {
        platformName = atrPlatformName;
    }

    // get set platformVersion
    public String getPlatformVersion(){
        return platformVersion;
    }
    public void setPlatformVersion(String atrPlatformVersion) {
        platformVersion = atrPlatformVersion;
    }

    // get set appPackage
    public String getAppPackage(){
        return appPackage;
    }
    public void setAppPackage(String atrAppPackage) {
        appPackage = atrAppPackage;
    }

    public String getAppActivity(){
        return appActivity;
    }
    public void setAppActivity(String atrAppActivity) {
        appActivity = atrAppActivity;
    }

    // get set appiumUrl
    public String getAppiumUrl(){
        return appiumUrl;
    }
    public void setAppiumUrl(String atrAppiumUrl) {
        appiumUrl = atrAppiumUrl;
    }

    public static void main(String[] args){

        AndroidDriver driver;

        String deviceName = "SM-J320FN";
        String uuid = "42000969c4967300";
        String platformName = "Android";
        String platformVersion = "5.1.1";
        String appPackage = "es.odilo.odilotest";
        String appActivity = "odilo.reader.main.view.MainActivity";
        String appiumUrl = "http://127.0.0.1:4723/wd/hub";

        try {
            DesiredCapabilities caps = new DesiredCapabilities();

            caps.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
            caps.setCapability(MobileCapabilityType.UDID, uuid);
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, platformName);
            caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
            caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, appPackage);
            caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, appActivity);

            // Appium URL
            URL url = new URL(appiumUrl);

            // driver = new AppiumDriver<MobileElement>(url, caps);
            driver = new AndroidDriver<MobileElement>(url, caps);
            // driver = new IOSDriver<MobileElement>(url, caps);

            System.out.println("Application started...");

        }catch (Exception exp) {
            System.out.println("Cause is: " +exp.getCause());
            System.out.println("Message is: " +exp.getMessage());
            exp.printStackTrace();
        }
    }

}
