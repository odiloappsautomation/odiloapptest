package es.odilo.odilotest.stepsDefinition.A_pantalla_Login;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class LoginNamekTest {

    public AppiumDriver<MobileElement> driver;
    public String user = "oscar";
    public String password = "Namek2021";

    @Given("I am at the login screen")
    public void iAmAtTheLoginScreen() throws MalformedURLException {

        try {
            DesiredCapabilities caps = new DesiredCapabilities();

            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Lenovo TB-8505F");
            caps.setCapability(MobileCapabilityType.UDID, "HA15HX5J");
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
            caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "10");

            caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "es.odilo.odilotest");
            caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "odilo.reader.main.view.MainActivity");

            URL url = new URL("http://127.0.0.1:4723/wd/hub");

            // driver = new AppiumDriver<MobileElement>(url, caps);
            driver = new AndroidDriver<MobileElement>(url, caps);
            // driver = new IOSDriver<MobileElement>(url, caps);

            System.out.println("Application started...");

        }catch (Exception exp) {
            System.out.println("Cause is: " +exp.getCause());
            System.out.println("Message is: " +exp.getMessage());
            exp.printStackTrace();
        }
    }

    @And("I select Namek library")
    public void iSelectNamekLibrary() {
        try{
            Thread.sleep(3000);

            // implicit wait
            WebDriverWait wait = new WebDriverWait(driver,15);
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

            // Library selection
            // abriendo desplegable de selección de biblioteca
            driver.findElement(By.id("es.odilo.odilotest:id/text_input_edit_text_search")).click();
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            // seleccionando bibliota
            driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[6]\n")).click();

            System.out.println("Biblioteca Namek RC seleccionada");


            WebDriverWait waitToClean = new WebDriverWait(driver, 15);
            waitToClean.until(ExpectedConditions.visibilityOfElementLocated(By.id("es.odilo.odilotest:id/edtUserName")));

            System.out.println("Campos de usuario  y contraseña cargados");


        }catch (Exception e){

            System.out.println("No se ha podido seleccionar la biblioteca Namek RC");
        }
    }

    @And("I try login KO with incorrect user id")
    public void iTryLoginKoWithIncorrectUserId() throws InterruptedException {

        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys("usuario_incorrecto"); // usuario incorrecto
        System.out.println("Usuario incorrecto introducido!");
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys(password); // textBox 'contraseña'
        System.out.println("Password correcto introducido!");

        Thread.sleep(3000);
    }

    @And("I try login KO with incorrect password")
    public void iTryLoginKoWithIncorrectPassword() throws InterruptedException {

        // rellenar datos de usuario para login
        //driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).clear(); // limpiar campo User Id
        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys(user); // usuario incorrecto
        System.out.println("Usuario correcto introducido!");
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).clear(); // limpiar campo de password
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys("password_incorrecto"); // textBox 'contraseña'
        System.out.println("Password incorrecto introducido!");

        Thread.sleep(3000);
    }

    @And("I try login KO with no terms")
    public void iTryLoginKoWithNoTerms() throws InterruptedException {

        // rellenar datos de usuario para login
        //driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).clear(); // limpiar campo User Id
        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys(user); // usuario incorrecto
        System.out.println("Usuario correcto introducido!");
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).clear(); // limpiar campo de password
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys(password); // textBox 'contraseña'
        System.out.println("Password correcto introducido!");

        Thread.sleep(3000);
    }

    @And("I try login Ok")
    public void iTryLoginOk() throws InterruptedException {

        Thread.sleep(3000);
        // rellenar datos de usuario para login
        //driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).clear(); // limpiar campo User Id
        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys(user); // usuario incorrecto
        System.out.println("Usuario correcto introducido!");
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).clear(); // limpiar campo de password
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys(password); // textBox 'contraseña'
        System.out.println("Password correcto introducido!");
        Thread.sleep(3000);
    }

    @When("I press the login button")
    public void iPressTheLoginButton() {

        driver.findElement(By.id("es.odilo.odilotest:id/btnActivate")).click(); // botón 'iniciar sesión'

        try {
            Thread.sleep(5000);
            System.out.println("Comprobando si salta el límite de dispositivos activos...");
            driver.findElement(By.id("android:id/button1")).click(); // cerrar mensaje de límite de dispositivos

        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("No se ha podido cerrar o no hay popup");
        }


    }

    @And("Check introductory page")
    public void checkIntroductoryPage(){
        try {
                Thread.sleep(3000);

                driver.findElement(By.id("es.odilo.odilotest:id/constraintGroup")).click();
                System.out.println("Página introductoria presente. Cerrar a mano.");
                driver.findElement(By.id("es.odilo.odilotest:id/close_button")).click();

        }catch (Exception intropage){

            System.out.println("No se ha encontrado página introductoria.");
        }

    }

    @Then("I am in the app")
    public void iAmInTheApp() {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); // esperando a que cargue la pantalla Inicio

        if(driver.findElement(By.id("es.odilo.odilotest:id/nav_catalog")).isSelected()){

            System.out.println("Bienvenido! Estás en tu Home!");

        }else if(driver.findElement(By.id("es.odilo.odilotest:id/nav_download")).isSelected()){

            System.out.println("Bienvenido! Estás en tu Estantería!");
        }
    }

    @And("I check and close the alert message")
    public void iCheckAndCloseTheAlertMessage() throws InterruptedException {

        Thread.sleep(3000);

        try {
            // comprobar el mensaje de alerta por usuario incorrecto
            System.out.println("Mensaje de alerta mostrado: " + driver.findElement(By.id("android:id/message")).getText());
            driver.findElement(By.id("android:id/button1")).click(); // botón 'Aceptar'

        }catch (Exception termsError){
            System.out.println("No se ha encontrado el mensaje de alerta de error");
        }
    }

    @And("I close the onboarding form if exist")
    public void iCloseTheOnboardingFormIfExist(){

        try{
            Thread.sleep(1000);
            driver.findElement(By.id("es.odilo.odilotest:id/close_button")).click();
            System.out.println("Completed Test 6 - Formulario de onboarding cerrado.");

        }catch (Exception onboardingException){

            System.out.println("Completed Test 6 - No se ha encontrado Formulario de onboarding...");
        }
    }

    @And("I close the introductory page if exist")
    public void iCloseTheIntroductoryPageIfExist(){

        try{

            Thread.sleep(1000);
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/header")).getText()); // text del título
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/txtDescription")).getText()); // text de descripción
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/image")).isDisplayed()); // hay imagen...
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/link_button")).getText()); // link a...

            driver.findElement(By.id("es.odilo.odilotest:id/close_button")).click();
            System.out.println("Completed Test 7 - Página introductoria cerrada correctamente.");

        }catch (Exception e){

            System.out.println("Completed Test 7 - no se ha encontrado Página introductoria");
        }
    }

    @And("I close the tutors form if exist")
    public void iCloseTheTutorsFormIfExist(){

        try{
            Thread.sleep(3000);

            driver.findElement(By.id("es.odilo.odilotest:id/btnConfirm")).click();
            System.out.println("Completed Test 8");

        }catch (Exception e){

            System.out.println("Completed Test 8 - no se ha encontrado campo tutor");
        }
    }

    @And("It takes you to the catalog screen")
    public void itTakesYouToTheCatalogScreen() {

        driver.findElement(By.id("es.odilo.odilotest:id/nav_catalog")).click(); // ir a pantalla Catálogo
    }

    @And("I check terms and conditions")
    public void iCheckTermsAndConditions() {

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // comprobar y marcar si no lo está marcado el check de términos y condiciones de uso
        if (driver.findElement(By.id("es.odilo.odilotest:id/check_box")).isSelected()){
            System.out.println("No hacer nada, ya estaba marcado");
        }else {
            // marcar check 'Términos y condiciones de uso'
            driver.findElement(By.id("es.odilo.odilotest:id/check_box")).click(); // textBox 'términos y condiciones de uso'
        }
    }

    @And("I uncheck terms and conditions")
    public void iUncheckTermsAndConditions() {
        // comprobar si está marcado el check de términos y condiciones de uso
        if (driver.findElement(By.id("es.odilo.odilotest:id/check_box")).isSelected()){
            // marcar check 'Términos y condiciones de uso'
            driver.findElement(By.id("es.odilo.odilotest:id/check_box")).click(); // textBox 'términos y condiciones de uso'
        }else {
            System.out.println("No hacer nada, ya estaba desmarcado");
        }
    }
}
