package es.odilo.odilotest.stepsDefinition;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import java.net.MalformedURLException;
import java.net.URL;

public class BaseClassTabletLenovo {

    // public AppiumDriver<MobileElement> driver;
    public AndroidDriver driver;

    @BeforeTest
    public void setup() throws MalformedURLException {

        try {
            DesiredCapabilities caps = new DesiredCapabilities();

            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Lenovo TB-8505F");
            caps.setCapability(MobileCapabilityType.UDID, "HA15HX5J");
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
            caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "10");
            caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "es.odilo.odilotest");
            caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "odilo.reader.main.view.MainActivity");

            URL url = new URL("http://127.0.0.1:4723/wd/hub");

            // driver = new AppiumDriver<MobileElement>(url, caps);
            driver = new AndroidDriver<MobileElement>(url, caps);
            // driver = new IOSDriver<MobileElement>(url, caps);

            System.out.println("Application started...");

        }catch (Exception exp) {
            System.out.println("Cause is: " +exp.getCause());
            System.out.println("Message is: " +exp.getMessage());
            exp.printStackTrace();
        }

    }

    @AfterSuite
    public void tearDown(){

        System.out.println("Fin de test");
        // driver.quit();
    }
}