package es.odilo.odilotest.stepsDefinition.Z_Smoke_Test;

import es.odilo.odilotest.stepsDefinition.BaseClassTabletLenovo;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

public class NamekSmokeTest extends BaseClassTabletLenovo {

    public String user = "osquitar";
    public String password = "Namek2021";
    public boolean malfunction;

    @Test(priority=1)
    @Given("I select library")
    public void iSelectLibrary() {

        // implicit wait
        WebDriverWait wait = new WebDriverWait(driver,15);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // Library selection
        // abriendo desplegable de selección de biblioteca
        driver.findElement(By.id("es.odilo.odilotest:id/text_input_edit_text_search")).click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        // seleccionando bibliota Namek DEV para checkear la página de mal funcionamiento
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[4]")).click();
        // seleccionando bibliota Namek RC
        //driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[5]")).click();


        try {
            // espera para la carga de la página de mal funcionamiento y
            // para que carguen los campos de usuario y password de la biblioteca correspondiente en caso de que no haya página de malfunction
            // implicit wait
            WebDriverWait waitForMalfunction = new WebDriverWait(driver,15);
            waitForMalfunction.until(ExpectedConditions.visibilityOfElementLocated(By.id("es.odilo.odilotest:id/close_button")));
            driver.findElement(By.id("es.odilo.odilotest:id/close_button")).click(); // user Id

            malfunction = true;
            System.out.println("Se ha mostrado y cerrado la página de malfunction");

        } catch (Exception e) {
            e.printStackTrace();
            malfunction = false;
            System.out.println("No se ha mostrado la página de malfunction");
        }

        // Continuamos para login
        // abre el desplegable de tipo de identificador
        driver.findElement(By.id("es.odilo.odilotest:id/spinner")).click();
        // selecciona el identificador tipo 7. Otro identificador
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[9]")).click();

        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys(user); // user Id
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys(password); // password
        driver.findElement(By.id("es.odilo.odilotest:id/check_box")).click(); // terms and conditions
        driver.findElement(By.id("es.odilo.odilotest:id/btnActivate")).click(); // login button

        System.out.println("Completed test...loginOk");
    }


    @Test (priority=2)
    @And("I close introductory page")
    public void iCloseIntroductoryPage(){

        if (malfunction == false){
            // pantalla de introducción del Onboarding form
            try{
                WebDriverWait wait = new WebDriverWait(driver, 15);
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("es.odilo.odilotest:id/clIntro")));

                System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/tvUser")).getText()); // Título del formulario
                System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/tvMessage")).getText()); // Contenido del formulario

                // abrir formulario
                driver.findElement(By.id("es.odilo.odilotest:id/btStart")).click();

                // opción no volver a mostrar el formulario de onboarding
                // driver.findElement(By.id("es.odilo.odilotest:id/tvNotShowEver")).click();

                System.out.println("Abriendo Formulario de... OK");

            }catch (Exception onboardingException){

                System.out.println("No se ha encontrado Formulario de onboarding...");
            }
        }else {
            System.out.println(malfunction);
        }


        System.out.println("Completed test... homeOnboardingForm");
    }

    @Test (priority=3)
    public void homeOnboardingForm(){

        // pantalla de introducción del Onboarding form
        try{
            WebDriverWait wait = new WebDriverWait(driver, 15);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("es.odilo.odilotest:id/clIntro")));

            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/tvUser")).getText()); // Título del formulario
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/tvMessage")).getText()); // Contenido del formulario

            // abrir formulario
            driver.findElement(By.id("es.odilo.odilotest:id/btStart")).click();

            // opción no volver a mostrar el formulario de onboarding
            // driver.findElement(By.id("es.odilo.odilotest:id/tvNotShowEver")).click();

            System.out.println("Abriendo Formulario de... OK");

        }catch (Exception onboardingException){

            System.out.println("No se ha encontrado Formulario de onboarding...");
        }

        System.out.println("Completed test... homeOnboardingForm");
    }

    @Test (priority=4)
    public void fillOnboardingForm(){

        // pantalla de introducción del Onboarding form
        try{
            System.out.println("Rellenando el Formulario de onboarding...");

            WebDriverWait wait = new WebDriverWait(driver, 15);
            wait.until(ExpectedConditions.elementToBeClickable(By.id("es.odilo.odilotest:id/ivRadioButton")));

            driver.findElement(By.id("es.odilo.odilotest:id/ivRadioButton")).click(); // seleccionar primer elemento del form
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            driver.findElement(By.id("es.odilo.odilotest:id/tvContinue")).click(); // se pulsa el botón continuar para pasar a la siguiente pregunta
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

            // mostrar por consola texto del form
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/tvDone")).getText());
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/tvFullFilled")).getText());

            // driver.findElement(By.id("es.odilo.odilotest:id/tvReturn")).click(); // se pulsa el botón... Atrás (por si se quiere regresar a la pantalla anterior)
            driver.findElement(By.id("es.odilo.odilotest:id/btSave")).click(); // guardar las respuestas

        }catch (Exception onboardingException){

            System.out.println("No se ha encontrado Formulario de onboarding...");
        }

        System.out.println("Completed test... fillOnboardingForm");
    }


    @Given("I am at the home screen")
    public void iAmAtTheHomeScreen() {

        driver.findElement(By.id("es.odilo.odilotest:id/nav_catalog")).click();

    }

    @And("Check for news banner")
    public void checkForNewsBanner() {

        // Comprobar si está presente el banner de noticias
        if (driver.findElement(By.id("es.odilo.odilotest:id/catalog_banner_container")).isDisplayed()){

            System.out.println("Branding con banner de noticias configurado!");

        }else {

            System.out.println("No se encuentran banner de noticias!");
        }
    }

    @And("Check for catalog be ready")
    public void checkForCatalogBeReady() {
        if (driver.findElement(By.id("es.odilo.odilotest:id/rvCatalog")).isDisplayed()){
            
        }
    }

    @And("Chek device limit popup")
    public void chekDeviceLimitPopup() {


        try{
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/alertTitle")).getText());
            System.out.println(driver.findElement(By.id("android:id/message")).getText());

            // desactivar primer dispositivo de la lista
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/action_bar")).getText()); // texto de la barra de título "Gestión de dispositivos"
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.Button")).click();
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            System.out.println(driver.findElement(By.id("android:id/message")).getText());
            driver.findElement(By.id("android:id/button1")).click();
            driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Atrás\"]")).click(); //botón atrás para volver a intentar el login
            driver.findElement(By.id("es.odilo.odilotest:id/btnActivate")).click();

        }catch (Exception deviceLimit){

            System.out.println("No se ha llegado al límite de dispositivos activos.");
        }
    }
}