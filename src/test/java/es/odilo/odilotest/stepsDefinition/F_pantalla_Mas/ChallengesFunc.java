package es.odilo.odilotest.stepsDefinition.F_pantalla_Mas;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class ChallengesFunc {

    public AppiumDriver<MobileElement> driver;
    public String user = "testdev_autouser";
    public String password = "Odilo2020";

    @Given("As a logged in user")
    public void asALoggedInUser(){

        // setup webdriver
        try {
            DesiredCapabilities caps = new DesiredCapabilities();

            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "sdk_gphone_x86_arm");
            caps.setCapability(MobileCapabilityType.UDID, "emulator-5554");
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
            caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "11");

            caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "es.odilo.odilotest");
            caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "odilo.reader.main.view.MainActivity");

            URL url = new URL("http://127.0.0.1:4723/wd/hub");

            // driver = new AppiumDriver<MobileElement>(url, caps);
            driver = new AndroidDriver<>(url, caps);
            // driver = new IOSDriver<MobileElement>(url, caps);

            System.out.println("Application started...");

        }catch (Exception exp) {
            System.out.println("Cause is: " +exp.getCause());
            System.out.println("Message is: " +exp.getMessage());
            exp.printStackTrace();
        }

        // seleccionar biblioteca test-developer
        WebDriverWait waitLoginPage = new WebDriverWait(driver, 10);
        waitLoginPage.until(ExpectedConditions.visibilityOfElementLocated(By.id("es.odilo.odilotest:id/spinner")));

        driver.findElement(By.id("es.odilo.odilotest:id/spinner")).click(); // abriendo desplegable de selección de biblioteca
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS); // esperando...
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[1]")).click(); // seleccionando biblioteca

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); // esperando...

        // limpiar y rellenar campo de texto datos de usuario
        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).clear(); // limpiar campo de texto
        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys(user); // usuario correcto

        // limpiar y rellenar campo de texto 'password'
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).clear(); // limpiar campo de password
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys(password); // contraseña correcta

        // comprobar y marcar si no lo está marcado el check de términos y condiciones de uso
        if (driver.findElement(By.id("es.odilo.odilotest:id/check_box")).isSelected()){

            System.out.println("the terms and conditions checkbox was already marked");

        }else {

            // marcar check 'Términos y condiciones de uso'
            driver.findElement(By.id("es.odilo.odilotest:id/check_box")).click(); // textBox 'términos y condiciones de uso'
        }

        // pulsar botón de inicio de sesión
        driver.findElement(By.id("es.odilo.odilotest:id/btnActivate")).click(); // botón 'iniciar sesión'

        // esperar página introductoria por si aparece y cerrarla
        try{

            // esperar y cerrar página introductoria si aparece
            System.out.println("Esperando página introductoria...");

            WebDriverWait waitIntroductoryPage = new WebDriverWait(driver, 5);
            waitIntroductoryPage.until(ExpectedConditions.visibilityOfElementLocated(By.id("es.odilo.odilotest:id/spinner")));

            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/header")).getText()); // text del título
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/txtDescription")).getText()); // text de descripción
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/image")).isDisplayed()); // hay imagen...
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/link_button")).getText()); // link a...

            driver.findElement(By.id("es.odilo.odilotest:id/close_button")).click();
            System.out.println("Página introductoria cerrada correctamente.");

        }catch (Exception e){

            System.out.println("No se ha encontrado Página introductoria");
        }
    }

    @And("I am at the More screen")
    public void iAmAtTheMoreScreen() {

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        driver.findElement(By.id("es.odilo.odilotest:id/nav_more")).click(); // ir a pantalla Más
    }

    @And("I look for the challenges option in the menu")
    public void iLookForTheChallengesOptionInTheMenu() {

        // FindElement
        MobileElement element = driver.findElement(MobileBy.AndroidUIAutomator(
                "new UiScrollable(new UiSelector().scrollable(true))" +
                        ".scrollIntoView(new UiSelector().resourceIdMatches(\"es.odilo.odilotest:id/nav_challenges\"))"));
    }

    @And("I go to the Challenges screen")
    public void IGoToTheChallengesScreen() {

        driver.findElement(By.id("es.odilo.odilotest:id/nav_challenges")).click(); // ir a pantalla Retos
    }

    @When("I create a new challenge")
    public void iCreateANewChallenge(){


        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.id("es.odilo.odilotest:id/floatingActionButton")).click(); // pulsar el botón para crear un nuevo reto
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);


        // comprobar si aparece el mensaje de alerta
        if (driver.findElement(By.id("es.odilo.odilotest:id/etChallengeName")).isEnabled()){

            driver.findElement(By.id("es.odilo.odilotest:id/etChallengeName")).sendKeys("reto personal"); // rellenar nombre del reto
            driver.findElement(By.id("es.odilo.odilotest:id/tvTypeHours")).click(); // sleccionar tipo de reto tipo 'cantidad de horas'
            driver.findElement(By.id("es.odilo.odilotest:id/etChallengeTarget")).sendKeys("1"); // rellenar cantiadad de horas
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

            driver.findElement(By.id("es.odilo.odilotest:id/lyFrequency")).click(); // abrir desplegable de frecuencia del reto
            driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[1]")).click(); // seleccionar 'día' como frecuencia del reto
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

            driver.findElement(By.id("es.odilo.odilotest:id/etEnding")).click(); // abrir desplegable de selección de fecha
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            driver.findElement(By.id("es.odilo.odilotest:id/next")).click(); // pulsando flecha para avanzar de mes y prevenir errores a la hora de seleccionar una fecha
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            driver.findElement(By.xpath("//android.widget.CheckedTextView[@content-desc=\"1\"]")).click(); // seleccionando el día 1 del mes siguiente al mes en curso para  prevenir errores a la hora de seleccionar una fecha
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            driver.findElement(By.id("es.odilo.odilotest:id/btnSelect")).click(); // pusar el botón 'seleccionar' para establecer la fecha del reto
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            driver.findElement(By.id("es.odilo.odilotest:id/btnSave")).click(); // pusar el botón 'Guardar' para crear el reto
            System.out.println("Se ha creado el reto correctamente.");

        }else {

            driver.findElement(By.id("es.odilo.odilotest:id/parentPanel"));
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/alertTitle")).getText());
            System.out.println(driver.findElement(By.id("android:id/message")).getText());

            driver.findElement(By.id("android:id/button1")).click(); // pulsar botón Aceptar para cerrar el popup de alerta

            driver.findElement(By.id("es.odilo.odilotest:id/ivRemove")).click(); // pulsar el botón para eliminar el reto creado
            driver.findElement(By.id("android:id/button1")).click(); // pulsar el botón Borrar para confirmar la eliminación del reto activo

            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

            // tras eliminar el reto activo, se procede a la creación de uno nuevo
            driver.findElement(By.id("es.odilo.odilotest:id/floatingActionButton")).click(); // pulsar el botón para crear un nuevo reto
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

            driver.findElement(By.id("es.odilo.odilotest:id/etChallengeName")).sendKeys("reto personal"); // rellenar nombre del reto

            driver.findElement(By.id("es.odilo.odilotest:id/tvTypeHours")).click(); // seleccionar tipo de reto tipo 'cantidad de horas'
            driver.findElement(By.id("es.odilo.odilotest:id/etChallengeTarget")).sendKeys("1"); // rellenar cantiadad de horas
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

            driver.findElement(By.id("es.odilo.odilotest:id/lyFrequency")).click(); // abrir desplegable de frecuencia del reto
            driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[1]")).click(); // seleccionar 'día' como frecuencia del reto
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

            driver.findElement(By.id("es.odilo.odilotest:id/etEnding")).click(); // abrir desplegable de selección de fecha
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            driver.findElement(By.id("es.odilo.odilotest:id/next")).click(); // pulsando flecha para avanzar de mes y prevenir errores a la hora de seleccionar una fecha
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            driver.findElement(By.xpath("//android.widget.CheckedTextView[@content-desc=\"1\"]")).click(); // seleccionando el día 1 del mes siguiente al mes en curso para  prevenir errores a la hora de seleccionar una fecha
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            driver.findElement(By.id("es.odilo.odilotest:id/btnSelect")).click(); // pusar el botón 'seleccionar' para establecer la fecha del reto
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            driver.findElement(By.id("es.odilo.odilotest:id/btnSave")).click(); // pusar el botón 'Guardar' para crear el reto
        }
    }

    @Then("The challenge created is displayed in the active challenges tab")
    public void theChallengeCreatedIsDisplayedInTheActiveChallengesTab() {

        if(driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View[1]/android.view.View/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout[2]/android.view.View/androidx.viewpager.widget.ViewPager/android.view.View/android.view.View/android.view.View")).isEnabled()){

            System.out.println("Nuevo Reto creado correctamente");
            System.out.println("Con título: " + driver.findElement(By.id("es.odilo.odilotest:id/tvName")).getText());
            System.out.println("Descripción: " + driver.findElement(By.id("es.odilo.odilotest:id/tvDescription")).getText());
            System.out.println("Tiempo restante: " + driver.findElement(By.id("es.odilo.odilotest:id/tvDaysRemaining")).getText());
            System.out.println("Creado por: " + driver.findElement(By.id("es.odilo.odilotest:id/tvCreatedBy")).getText());
            System.out.println("Fecha de inicio: " + driver.findElement(By.id("es.odilo.odilotest:id/tvStartDate")).getText());

        }else{

            System.out.println("No se ha podido crear el reto.");
        }
    }

    @And("I remove the active challenge")
    public void iRemoveTheActiveChallenge(){

        try{

            driver.findElement(By.id("es.odilo.odilotest:id/ivRemove")).click(); // pulsar el botón para eliminar el reto creado
            driver.findElement(By.id("android:id/button1")).click(); // pulsar el botón Borrar para confirmar la eliminación del reto activo

        }catch (Exception e){

            System.out.println("No se ha encontrado ningún reto activo.");
        }
    }

    @Then("Then The challenge is removed from the active challenges tab")
    public void thenTheChallengeIsRemovedFromTheActiveChallengesTab(){

        System.out.println("Reto eliminado correctamente.");
    }
}
