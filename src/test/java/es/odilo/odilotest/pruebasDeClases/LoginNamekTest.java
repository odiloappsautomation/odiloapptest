package es.odilo.odilotest.pruebasDeClases;

import es.odilo.odilotest.stepsDefinition.BaseClassTabletLenovo;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

public class LoginNamekTest extends BaseClassTabletLenovo {

    public String user = "oscar";
    public String password = "Namek2021";

    @Test (priority=1)
    public void loginOk() {

        // implicit wait
        WebDriverWait wait = new WebDriverWait(driver,15);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        // Library selection
        // abriendo desplegable de selección de biblioteca
        driver.findElement(By.id("es.odilo.odilotest:id/text_input_edit_text_search")).click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        // seleccionando bibliota Namek RC
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[5]")).click();
        // espera para que carguen los campos de usuario y password de la biblioteca correspondiente
        wait.until(ExpectedConditions.elementToBeClickable(By.id("es.odilo.odilotest:id/edtUserName")));

        // Comprobar si hay o no página de malfunction (si está habilitada debe salir tras selecciona la OTK del desplegable de selección de biblioteca
        try{
            WebDriverWait waitIntroPage = new WebDriverWait(driver, 10);
            waitIntroPage.until(ExpectedConditions.visibilityOfElementLocated(By.id("es.odilo.odilotest:id/constraintGroup")));

            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/header")).getText());
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/txtDescription")).getText());
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/image")).isDisplayed());
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/link_button")).getText());
            if(driver.findElement(By.id("es.odilo.odilotest:id/progressbar")).isDisplayed()){
                System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/progressbar")).isDisplayed());
                System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/progressbar")).getText());
            }else{
                System.out.println("No hay barra de progreso de tiempo");
            }
        }catch (Exception onboardingException){

            System.out.println("No se ha encontrado Página introductoria...");
        }

        // Rellenando campos para login CORRECTO
        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys(user); // user Id
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys(password); // password
        driver.findElement(By.id("es.odilo.odilotest:id/check_box")).click(); // terms and conditions
        driver.findElement(By.id("es.odilo.odilotest:id/btnActivate")).click(); // login button

        System.out.println("Completed test...loginOk");
    }

    @Test (priority=2)
    public void checkOnboardingForm(){

        // comprobar si aparece o no el formulario de onboarding
        try{
            WebDriverWait wait = new WebDriverWait(driver, 15);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("es.odilo.odilotest:id/clIntro")));

            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/tvUser")).getText()); // Título del formulario
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/tvMessage")).getText()); // Contenido del formulario

            // abrir formulario
            driver.findElement(By.id("es.odilo.odilotest:id/btStart")).click();

            // opción no volver a mostrar el formulario de onboarding
            // driver.findElement(By.id("es.odilo.odilotest:id/tvNotShowEver")).click();

            System.out.println("Abriendo Formulario de... OK");

        }catch (Exception onboardingException){

            System.out.println("No se ha encontrado Formulario de onboarding...");
        }

        System.out.println("Completed test... homeOnboardingForm");
    }

    @Test (priority=3)
    public void fillOnboardingForm(){

        // pantalla de introducción del Onboarding form
        try{
            System.out.println("Rellenando el Formulario de onboarding...");

            WebDriverWait wait = new WebDriverWait(driver, 15);
            wait.until(ExpectedConditions.elementToBeClickable(By.id("es.odilo.odilotest:id/ivRadioButton")));

            driver.findElement(By.id("es.odilo.odilotest:id/ivRadioButton")).click(); // seleccionar primer elemento del form
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            driver.findElement(By.id("es.odilo.odilotest:id/tvContinue")).click(); // se pulsa el botón continuar para pasar a la siguiente pregunta
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

            // mostrar por consola texto del form
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/tvDone")).getText());
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/tvFullFilled")).getText());

            // driver.findElement(By.id("es.odilo.odilotest:id/tvReturn")).click(); // se pulsa el botón... Atrás (por si se quiere regresar a la pantalla anterior)
            driver.findElement(By.id("es.odilo.odilotest:id/btSave")).click(); // guardar las respuestas

        }catch (Exception onboardingException){

            System.out.println("No se ha encontrado Formulario de onboarding...");
        }

        System.out.println("Completed test... fillOnboardingForm");
    }

}
