package es.odilo.odilotest.pruebasDeClases;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;
import java.net.URL;

public class BeforeTestMobileCaps {

    // Clase para establecer las capabilities escogiendo de un listado (Clase BeforeTestMobileCaps)
    // Se hace de este modo con el fin de poder ir añadiendo diferentes móviles sin tener que modificar a mano cada
    // vez que se quiera probar los test en un dispositivo diferente
    /*
    public void setupLenovoTablet() throws MalformedURLException {

        try {
            DesiredCapabilities caps = new DesiredCapabilities();

            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Lenovo TB-8505F");
            caps.setCapability(MobileCapabilityType.UDID, "HA15HX5J");
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
            caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "10");

            caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "es.odilo.odilotest");
            caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "odilo.reader.main.view.MainActivity");

            URL url = new URL("http://127.0.0.1:4723/wd/hub");

            // driver = new AppiumDriver<MobileElement>(url, caps);
            driver = new AndroidDriver<MobileElement>(url, caps);
            // driver = new IOSDriver<MobileElement>(url, caps);

            System.out.println("Application started...");

        }catch (Exception exp) {
            System.out.println("Cause is: " +exp.getCause());
            System.out.println("Message is: " +exp.getMessage());
            exp.printStackTrace();
        }
    }
    */

    /*
    public void setupSamsungMobile() throws MalformedURLException {

        try {
            DesiredCapabilities caps = new DesiredCapabilities();

            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "SM-J320FN");
            caps.setCapability(MobileCapabilityType.UDID, "42000969c4967300");
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
            caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "5.1.1");

            caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "es.odilo.odilotest");
            caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "odilo.reader.main.view.MainActivity");

            URL url = new URL("http://127.0.0.1:4723/wd/hub");

            // driver = new AppiumDriver<MobileElement>(url, caps);
            driver = new AndroidDriver<MobileElement>(url, caps);
            // driver = new IOSDriver<MobileElement>(url, caps);

            System.out.println("Application started...");

        }catch (Exception exp) {
            System.out.println("Cause is: " +exp.getCause());
            System.out.println("Message is: " +exp.getMessage());
            exp.printStackTrace();
        }
    }
     */
}
