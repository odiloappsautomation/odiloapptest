package es.odilo.odilotest.pruebasDeClases;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseClassTestotkWithLogin {

    // AppiumDriver<MobileElement> driver;
    AndroidDriver driver;

    public String user = "testdev_14";
    public String password = "Odilo2020";


    @BeforeTest
    public void setup() throws MalformedURLException {

        try {
            DesiredCapabilities caps = new DesiredCapabilities();

            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Lenovo TB-8505F");
            caps.setCapability(MobileCapabilityType.UDID, "HA15HX5J");
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
            caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "10");

            caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "es.odilo.odilotest");
            caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "odilo.reader.main.view.MainActivity");

            URL url = new URL("http://127.0.0.1:4723/wd/hub");

            // driver = new AppiumDriver<MobileElement>(url, caps);
            driver = new AndroidDriver<MobileElement>(url, caps);
            // driver = new IOSDriver<MobileElement>(url, caps);

            System.out.println("Application started...");

        }catch (Exception exp) {
            System.out.println("Cause is: " +exp.getCause());
            System.out.println("Message is: " +exp.getMessage());
            exp.printStackTrace();
        }
    }

    @Test (priority = 1, description = "Login correcto en test-developer")
    public void loginTestDev(){

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("es.odilo.odilotest:id/spinner")));

        try{
            driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys(user); // user Id
            driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys(password); // password
            driver.findElement(By.id("es.odilo.odilotest:id/check_box")).click(); // terms and conditions
            driver.findElement(By.id("es.odilo.odilotest:id/btnActivate")).click(); // login button

        }catch (Exception e){
            System.out.println(e.getMessage());
            System.out.println("Completed test de BaseClassWithLogin");
        }
    }

    @AfterSuite
    public void tearDown(){

        System.out.println("Fin de test from BaseClassWithLogin");
        driver.quit();
    }
}
