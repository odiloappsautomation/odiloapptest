package es.odilo.odilotest.pruebasDeClases;

import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.net.MalformedURLException;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class BrowserStackSample {

	public static void main(String[] args) throws MalformedURLException, InterruptedException {

		String user = "testdev_11";
		String password = "Odilo2020";

		DesiredCapabilities caps = new DesiredCapabilities();

		// Set your access credentials
		caps.setCapability("browserstack.user", "scarquiles1");
		caps.setCapability("browserstack.key", "Voq97ZCspUjZJc55aqBZ");

		// Set URL of the application under test
		caps.setCapability("app", "bs://ae42625f789c952051ea8bc95ede49710fc2598f");

		// Specify device and os_version for testing
		caps.setCapability("device", "Google Pixel 3");
		caps.setCapability("os_version", "9.0");

		// Set other BrowserStack capabilities
		caps.setCapability("project", "First Java Project");
		caps.setCapability("build", "Java Android");
		caps.setCapability("name", "first_test");

		// Initialise the remote Webdriver using BrowserStack remote URL
		// and desired capabilities defined above
		AndroidDriver<AndroidElement> driver = new AndroidDriver<AndroidElement>(
				new URL("http://hub.browserstack.com/wd/hub"), caps);

		/* Login KO usuario incorrecto */
		// explicit wait
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("es.odilo.odilotest:id/spinner")));

		// Library selection
		driver.findElement(By.id("es.odilo.odilotest:id/spinner")).click();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]")).click();

		// rellenar datos de usuario para login
		wait.until(ExpectedConditions.elementToBeClickable(By.id("es.odilo.odilotest:id/edtUserName")));
		driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys("usuarioIncorrecto"); // User Id
		System.out.println("incorrect user id");
		driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys(password); // textBox 'contraseña'
		System.out.println("correct password");
		driver.findElement(By.id("es.odilo.odilotest:id/check_box")).click(); // textBox 'términos y condiciones de uso'
		driver.findElement(By.id("es.odilo.odilotest:id/btnActivate")).click(); // botón 'iniciar sesión'
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		driver.findElement(By.id("android:id/button1")).click(); // botón 'Aceptar'
		System.out.println("Completed test...1 - login Ko Incorrect User Id  \n");

		/* Login KO password incorrecto */
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys(user); // user Id
		System.out.println("correct user id");
		driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys("passwordIncorrecto"); // password
		System.out.println("incorrect password");
		driver.findElement(By.id("es.odilo.odilotest:id/check_box")).click(); // terms and conditions
		driver.findElement(By.id("es.odilo.odilotest:id/btnActivate")).click(); // Login button
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.findElement(By.id("android:id/button1")).click(); // botón 'Aceptar'
		System.out.println("Completed test...2 - login Ko Incorrect Password  \n");

		/* Login KO Terms and conditions unchecked */
		driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys(user); // User Id
		System.out.println("correct user id");
		driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys(password); // Password
		System.out.println("correct password");
		System.out.println("terms and conditions unchecked");
		driver.findElement(By.id("es.odilo.odilotest:id/btnActivate")).click(); // Login button
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.findElement(By.id("android:id/button1")).click(); // botón 'Aceptar'
		System.out.println("alerta cerrada");
		System.out.println("Completed test...3 - login Ko Terms And Conditions \n");

		/* Login OK */
		driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys(user); // user Id
		driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys(password); // password
		driver.findElement(By.id("es.odilo.odilotest:id/check_box")).click(); // terms and conditions
		driver.findElement(By.id("es.odilo.odilotest:id/btnActivate")).click(); // login button
		System.out.println("Completed test...4 - login Ok  \n");

		/* Close onboarding form */
		try{
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			driver.findElement(By.id("es.odilo.odilotest:id/close_button")).click();
			System.out.println("Completed Test 5 - Formulario de onboarding cerrado... OK");
		}catch (Exception onboardingException){
			System.out.println("Completed Test 5 - No se ha encontrado Formulario de onboarding...");
		}

		/* Close introductory page */
		try{
			// try to close intro page if exits
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			driver.findElement(By.id("es.odilo.odilotest:id/close_button")).click();
			System.out.println("Completed test...6 - Se cierra la página introductoria correctamente. \n");
		}catch (Exception e){
			System.out.println("Completed test...6 - no se ha encontrado Página introductoria");
		}

		/* Tutors fill */
		try{
			WebDriverWait waitToTutors = new WebDriverWait(driver,30);
			waitToTutors.until(ExpectedConditions.elementToBeClickable(By.id("es.odilo.odilotest:id/tutorEmail"))).sendKeys("oquiles@odilo.us");
			driver.findElement(By.id("es.odilo.odilotest:id/btnConfirm")).click();
			System.out.println("Completed test...7 - se rellena y envía el Tutor  \n");
		}catch (Exception e){
			System.out.println("Completed test...7 - No se ha encontrado formulario de tutores que rellenar...");
		}

		try{
			/* Ir a pantalla Cuenta */
			driver.findElement(By.id("es.odilo.odilotest:id/nav_more")).click(); // ir a pantalla Más
			driver.findElement(By.id("es.odilo.odilotest:id/account")).click(); // ir a pantalla Cuenta
			// try to close user session
			WebDriverWait waitToVloseBtn = new WebDriverWait(driver,15);
			waitToVloseBtn.until(ExpectedConditions.elementToBeClickable(By.id("es.odilo.odilotest:id/continueBtn")));
			driver.findElement(By.id("es.odilo.odilotest:id/continueBtn")).click();
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			driver.findElement(By.id("android:id/button1")).click();
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			driver.findElement(By.id("android:id/button1")).click();
			System.out.println("Completed Test 9 - sesión de usuario cerrada correctamente");
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		}catch (Exception e){
			System.out.println("Completed Test 9 - no se ha podido cerrar la sesión de usuario.");
		}


		// Invoke driver.quit() after the test is done to indicate that the test is completed.
		driver.quit();
	}
}