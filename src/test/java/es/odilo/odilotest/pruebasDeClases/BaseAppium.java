package es.odilo.odilotest.pruebasDeClases;

import com.tngtech.propertyloader.PropertyLoader;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.WebElement;

import java.io.File;

public class BaseAppium {

    public static AppiumDriverLocalService service;
    public static AppiumDriver<WebElement> driver;

    public void init() throws Exception {

        // carga del fichero .properties
        PropertyLoader loadproperty = new PropertyLoader();

        // Recuperación del fichero de propiedades de la ruta y nombre de la aplicación móvil
        File app = new File(loadproperty.load().getProperty("apkDir"), loadproperty.load().getProperty("apkName"));

        // Generación de las capabilities a nivel del servicio de Appium


    }

}
