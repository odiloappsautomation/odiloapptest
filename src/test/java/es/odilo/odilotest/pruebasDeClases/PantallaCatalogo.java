package es.odilo.odilotest.pruebasDeClases;

import es.odilo.odilotest.stepsDefinition.BaseClassTabletLenovo;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class PantallaCatalogo extends BaseClassTabletLenovo {

    /*
    @Test (priority = 1)
    public class A {
        private BaseLoginClass = new BaseLoginClass();
        public void loginFromOtherClass() {
            loginTest.loginTestDeveloperOk();
        }
    }
    */

    @Test (priority=2)
    public void introductoryPage(){

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

        try{
            // try to close intro page if exits
            driver.findElement(By.id("es.odilo.odilotest:id/close_button")).click();

        }catch (Exception e){

            System.out.println("no se ha encontrado Página introductoria");
        }

        System.out.println("Completed test...6 - cerrar página introductoria");

    }

    @Test (priority=3)
    public void goToCatalog(){

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

        driver.findElement(By.id("es.odilo.odilotest:id/nav_catalog")).click();
    }

    @Test (priority=4)
    public void checkBanners(){

        System.out.println("mostrando el contenido del contenedor de banners " + driver.findElement(By.id("es.odilo.odilotest:id/catalog_banner_container")).getText());
    }

    @Test (priority=5)
    public void goInCarousels(){

        System.out.println("Accediendo al carrusel " + driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Novedades\"]")).getText());
        driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Novedades\"]")).click();
    }

}
