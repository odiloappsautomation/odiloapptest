package es.odilo.odilotest.pruebasDeClases;

import es.odilo.odilotest.stepsDefinition.BaseClassTabletLenovo;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class LoginTestOtk extends BaseClassTabletLenovo {

    public String user = "testoq11";
    public String password = "Namek2021";

    @Test (priority=1, description = "Test 1 - login test by entering wrong Userid")
    public void loginKoIncorrectUserId() throws InterruptedException {

        WebDriverWait waitLoginPage = new WebDriverWait(driver, 15);
        waitLoginPage.until(ExpectedConditions.visibilityOfElementLocated(By.id("es.odilo.odilotest:id/spinner")));

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); // esperando a que cargue la pantalla de login
        driver.findElement(By.id("es.odilo.odilotest:id/spinner")).click(); // abriendo desplegable de selección de biblioteca
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]")).click(); // seleccionando biblioteca


        // limpiar y rellenar campo de texto 'Id de usuario'
        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).clear(); // limpiar campo de texto
        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys("usuarioIncorrecto"); // usuario incorrecto

        // limpiar y rellenar campo de texto 'password'
        //driver.findElement(By.id("es.odilo.odilotest:id/text_input_end_icon")).click();
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).clear(); // limpiar campo de password
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys(password); // textBox 'contraseña'

        // comprobar si está marcado el check de términos y condiciones de uso
        if (!driver.findElement(By.id("es.odilo.odilotest:id/check_box")).isSelected()){

            // marcar check 'Términos y condiciones de uso'
            driver.findElement(By.id("es.odilo.odilotest:id/check_box")).click(); // textBox 'términos y condiciones de uso'

        }else {

            System.out.println("Ya está marcado el check de Términos y condiciones de uso...");
        }
        driver.findElement(By.id("es.odilo.odilotest:id/btnActivate")).click(); // botón 'iniciar sesión'

        Thread.sleep(3000);

        // comprobar el mensaje de alerta por usuario incorrecto
        System.out.println("Mensaje de alerta mostrado: " + driver.findElement(By.id("android:id/message")).getText());
        driver.findElement(By.id("android:id/button1")).click(); // botón 'Aceptar'
        System.out.println("Completed Test 1 - usuario incorrecto");

    }

    @Test (priority=3, description = "Test 2 - login test by entering wrong password")
    public void loginKoIncorrectPass() throws InterruptedException {

        Thread.sleep(3000);

        // rellenar datos de usuario para login
        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).clear(); // limpiar campo de texto
        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys(user); // usuario incorrecto

        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).clear(); // limpiar campo de password
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys("incorrect********"); // textBox 'contraseña'

        driver.findElement(By.id("es.odilo.odilotest:id/btnActivate")).click(); // botón 'iniciar sesión'

        Thread.sleep(3000);

        // comprobar el mensaje de alerta por usuario incorrecto
        System.out.println("Mensaje de alerta mostrado: " + driver.findElement(By.id("android:id/message")).getText());
        driver.findElement(By.id("android:id/button1")).click(); // botón 'Aceptar'
        System.out.println("Completed Test 2 - contraseña incorrecta");
    }

    @Test (priority=4, description = "Test 3 - login test by not check terms and conditions")
    public void loginKoTermsAndConditions() throws InterruptedException {

        Thread.sleep(3000);

        // rellenar datos de usuario para login
        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).clear(); // limpiar campo de texto
        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys(user); // usuario correcto
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).clear(); // limpiar campo de password
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys(password); // contraseña correcta
        driver.findElement(By.id("es.odilo.odilotest:id/check_box")).click(); // textBox 'términos y condiciones de uso'
        driver.findElement(By.id("es.odilo.odilotest:id/btnActivate")).click(); // botón 'iniciar sesión'

        Thread.sleep(3000);

        // cerrar mensaje de alerta
        driver.findElement(By.id("android:id/button1")).click(); // botón 'Aceptar'
        System.out.println("Completed Test 3 - Términos y condiciones de uso sin marcar");
    }

    @Test (priority=5, description = "Test 4 - Login OK")
    public void loginOk() throws InterruptedException {

        Thread.sleep(3000);

        // rellenar datos de usuario para login
        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).clear(); // limpiar campo de texto
        driver.findElement(By.id("es.odilo.odilotest:id/edtUserName")).sendKeys(user); // usuario correcto
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).clear(); // limpiar campo de password
        driver.findElement(By.id("es.odilo.odilotest:id/edtPassword")).sendKeys(password); // contraseña correcta

        // comprobar si está marcado el check de términos y condiciones de uso
        if (!driver.findElement(By.id("es.odilo.odilotest:id/check_box")).isSelected()){

            driver.findElement(By.id("es.odilo.odilotest:id/check_box")).click(); // textBox 'términos y condiciones de uso'

        }else {

            System.out.println("Ya está marcado el check de Términos y condiciones de uso...");
        }

        driver.findElement(By.id("es.odilo.odilotest:id/btnActivate")).click(); // botón 'iniciar sesión'
        System.out.println("Completed Test 5 - Login correcto");
    }

    // Comentado para probar la clase OnboardingForm, pendiente crear clase StepDefinitions
    @Test (priority=6, description = "Test 6 - Trying to close onboarding form")
    public void closeOnboardingForm() throws InterruptedException {

        try{

            Thread.sleep(1000);
            driver.findElement(By.id("es.odilo.odilotest:id/close_button")).click();
            System.out.println("Completed Test 6 - Formulario de onboarding cerrado.");

        }catch (Exception onboardingException){

            System.out.println("Completed Test 6 - No se ha encontrado Formulario de onboarding...");
        }
    }

    @Test (priority=7, description = "Test 7 - Trying to close Introductory page")
    public void closeIntroductoryPage() throws InterruptedException {

        try{

            Thread.sleep(1000);
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/header")).getText()); // text del título
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/txtDescription")).getText()); // text de descripción
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/image")).isDisplayed()); // hay imagen...
            System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/link_button")).getText()); // link a...

            driver.findElement(By.id("es.odilo.odilotest:id/close_button")).click();
            System.out.println("Completed Test 7 - Página introductoria cerrada correctamente.");

        }catch (Exception e){

            System.out.println("Completed Test 7 - no se ha encontrado Página introductoria");
        }
    }

    @Test (priority = 8, description = "Test 8 - Fill out tutor form")
    public void tutorsFill() throws InterruptedException {

        try{
            Thread.sleep(3000);

            driver.findElement(By.id("es.odilo.odilotest:id/btnConfirm")).click();
            System.out.println("Completed Test 8");

        }catch (Exception e){

            System.out.println("Completed Test 8 - no se ha encontrado campo tutor");
        }
    }

    @Test (priority = 9, description = "Test 9 - Logout OK")
    public void logOut() throws InterruptedException {

        driver.findElement(By.id("es.odilo.odilotest:id/nav_more")).click(); // ir a pantalla Más

        // FindElement
        MobileElement element = (MobileElement) driver.findElement(MobileBy.AndroidUIAutomator(
                "new UiScrollable(new UiSelector().scrollable(true))" +
                        ".scrollIntoView(new UiSelector().resourceIdMatches(\"es.odilo.odilotest:id/account\"))"));

        driver.findElement(By.id("es.odilo.odilotest:id/account")).click(); // ir a pantalla Cuenta

        // cerrar sesión
        Thread.sleep(1000);
        driver.findElement(By.id("es.odilo.odilotest:id/continueBtn")).click(); // try to close user session
        Thread.sleep(3000);

        System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/alertTitle")).getText());
        System.out.println(driver.findElement(By.id("android:id/message")).getText());

        driver.findElement(By.id("android:id/button1")).click();

        // realizar una captura de pantalla
        try{

            File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

            FileUtils.copyFile(src, new File("C:\\Automation\\projectScreenshots\\imagen1.png"));

        }catch (IOException e){

            System.out.println(e.getMessage());
        }
    }
}