package es.odilo.odilotest.pruebasDeClases;

import es.odilo.odilotest.stepsDefinition.BaseClassTabletLenovo;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;


public class SelfRegistration extends BaseClassTabletLenovo {

    @Test (priority = 1)
    public void selectLibrary() {

        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("es.odilo.odilotest:id/spinner")));

        // Library selection
        driver.findElement(By.id("es.odilo.odilotest:id/spinner")).click();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]")).click();
    }

    @Test (priority = 2)
    public void checkRequieredFieldsKo(){

        // TODO:
    }

    @Test (priority = 3)
    public void checkTermsKo(){

        // TODO:
    }

    @Test (priority = 4)
    public void endRegistration(){

        // TODO:
    }
}
