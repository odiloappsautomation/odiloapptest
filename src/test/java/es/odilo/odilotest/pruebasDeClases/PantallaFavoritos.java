package es.odilo.odilotest.pruebasDeClases;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class PantallaFavoritos  extends BaseClassTestotkWithLogin {

    @Test(priority = 1, description = "")
    public void goToNavFavorites(){

        try {

            driver.findElement(By.id("es.odilo.odilotest:id/nav_favorites")).click();

            System.out.println(driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.widget.TextView")).getText());

        }catch (Exception e){

            System.out.println("Probablemente aún no se haya iniciado sesión. Comprobar...");
            driver.quit();
        }
    }

    @Test (priority = 2, description = "")
    public void checkFavorits(){

        System.out.println(driver.findElement(By.id("es.odilo.odilotest:id/view_container")).findElements(By.id("es.odilo.odilotest:id/container_list_item")));
        // driver.findElement(By.id("es.odilo.odilotest:id/view_container")).findElements(By.id("es.odilo.odilotest:id/container_list_item"));
    }
}
