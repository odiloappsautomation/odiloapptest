Feature: Test Challenges Functionality
  Test the functionality of public and private challenges

  @TestingChallenges
  Scenario: comprobar y eliminar si hay retos activos

    Given I am at the More screen
    And I look for the challenges option in the menu
    And I go to the Challenges screen
    And I remove the active challenge


  @TestingChallenges
  Scenario: as a user without active challenges
  Go to the challenge screen and create a new challenge

    Given I am at the More screen
    And I look for the challenges option in the menu
    And I go to the Challenges screen
    When I create a new challenge
    Then The challenge created is displayed in the active challenges tab

  @TestingChallenges
  Scenario: as a user with active challenges
  Go to the challenge screen and delete an existing challenge

    Given I am at the More screen
    And I look for the challenges option in the menu
    And I go to the Challenges screen
    When I remove the active challenge
    Then Then The challenge is removed from the active challenges tab