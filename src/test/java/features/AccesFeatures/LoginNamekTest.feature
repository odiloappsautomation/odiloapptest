Feature: Smoke-test Namek RC
  Check bad login message for bad user
  Check bad login message for bad password
  Check bad login message for bad user
  Check incorrect login message when not accepting the terms and conditions of use

  Background:
    Given I am at the login screen
    And I select Namek library

  Scenario: Login KO incorrect user id
    Given I try login KO with incorrect user id
    And I check terms and conditions
    When I press the login button
    Then I check and close the alert message

  Scenario: Login KO incorrect password
    Given I try login KO with incorrect password
    And I check terms and conditions
    When I press the login button
    Then I check and close the alert message

  Scenario: Login KO terms and conditions unchecked
    Given I try login KO with no terms
    And I uncheck terms and conditions
    When I press the login button
    Then I check and close the alert message

  Scenario: Login testotk OK
    Given I try login Ok
    And I check terms and conditions
    And I press the login button
    When Check introductory page
    Then I am on the home screen