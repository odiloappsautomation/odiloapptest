Feature: Test-Developer login functionality
  Check bad login message for bad user
  Check bad login message for bad password
  Check bad login message for bad user
  Check incorrect login message when not accepting the terms and conditions of use

  Background:
    Given I am at the login screen
    And I select test-developer library

  @LoginCheck
  Scenario: Login KO incorrect user id
    Given I entered an incorrect user id
    And I enter my password
    And Check Terms and conditions
    When I press the login button
    Then I check and close the alert message

  @LoginCheck
  Scenario: Login KO incorrect password
    Given I enter the user id
    And I entered an incorrect password
    And Check Terms and conditions
    When I press the login button
    Then I check and close the alert message

  @LoginCheck
  Scenario: Login KO terms and conditions unchecked
    Given I enter the user id
    And I enter my password
    And Uncheck Terms and conditions
    When I press the login button
    Then I check and close the alert message

  @SmokeTest
  Scenario: Login testotk OK
    Given I enter the user id
    And I enter my password
    And Check Terms and conditions
    When I press the login button
    Then I close the onboarding form if exist
    And I close the introductory page if exist
    When I close the tutors form if exist
    Then It takes you to the catalog screen