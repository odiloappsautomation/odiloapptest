Feature: Smoke-test Namek RC
  Check critical functionalities
  User acceptance test

  Background:
    Given I am at the login screen
    And I select Namek RC library

  Scenario: Check Login screen
    Given I select library
    And I check terms and conditions
    And I press the login button
    And Chek device limit popup
    And Check introductory page
    Then I am in the app

    Scenario: Check Home screen
      Given I am at the home screen
      And Check for news banner
      And Check for catalog be ready
      And Check the first carousel
      And I access inside
      And I access inside a first resource
